<footer class="footer">
    <div class="navbar">
        <p class="pull-right"><a href="#"><?= __('Back to top') ?></a></p>
        <p class="text-center"><?= __('{0} Magic Mirror', ['Chromecast']) ?> - MIT License | <a href="https://github.com/TheChillerCraft/MagicMirror">GitHub Page</a></p>
    </div>
</footer>